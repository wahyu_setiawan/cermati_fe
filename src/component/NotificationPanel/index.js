import React from 'react'
import PropTypes from 'prop-types'
import './styles.css'

const NotificationPanel = ({ clicked, visibility }) => {

  const [height, setHeight] = React.useState()

  React.useEffect(() => {
    const elnotif = document.getElementById('notification-id')
    if (visibility) {
      setHeight(elnotif.scrollHeight)
    } else {
      setHeight(0)
    }
  }, [visibility, setHeight])

  return (
    <div
      id="notification-id"
      style={{ maxHeight: `${height}` }}
      className={`notification-container`}>
      <div className="notification-content">
        <div className="notification-content-link">
          By accessing and using this website, you acknowledge that you have read and understand our
          <a href="https://cermati.com" target="_blank" rel="noreferrer noopener"> Cookie Policy</a>,
          <a href="https://cermati.com" target="_blank" rel="noreferrer noopener"> Privacy Policy </a> and our
          <a href="https://cermati.com" target="_blank" rel="noreferrer noopener"> Term of service</a>.
        </div>
        <div className="notification-content-button">
          <div className="btn btn-primary" onClick={clicked}>
            Got it
          </div>
        </div>
      </div>
    </div>
  )
}

export default NotificationPanel

NotificationPanel.propTypes = {
  clicked: PropTypes.func.isRequired,
  visibility: PropTypes.bool.isRequired
}
