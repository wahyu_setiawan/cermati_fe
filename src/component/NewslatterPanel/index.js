import React from 'react'
import Card from '../../libs/CardComponent'
import './styles.css'

const NewsLetter = () => {
  const storage = window.localStorage
  const [input, setInput] = React.useState('')
  const [hide, setHide] = React.useState(true)
  const [yTranslate, setYTranslate] = React.useState('')
  const node = React.useRef()

  React.useEffect(() => {
    window.addEventListener('scroll', () => {

      const yScroll = window.innerHeight + document.documentElement.scrollTop
      const pOffset = document.documentElement.offsetHeight - 100
      const panelConfig = JSON.parse(storage.getItem('panel'))
      const now = new Date()
      const minutes = now.setMinutes(now.getMinutes())

      if (!panelConfig) {
        if (yScroll > pOffset) {
          setHide(h => h = false)
          setYTranslate(`-${node.current.scrollHeight}`)
        }
      } else if (panelConfig && (minutes > panelConfig.expired)) {
        storage.clear()
        if (yScroll > pOffset) {
          setHide(h => h = false)
          setYTranslate(`-${node.current.scrollHeight}`)
        }
      } else if (panelConfig && panelConfig.subscribe) {
        setHide(h => h = true)
        setYTranslate(`${node.current.scrollHeight}`)
      }
    })
  }, [setHide, storage, setYTranslate])

  const handleOnChange = e => setInput(e.target.value)

  const handleFormSubmit = e => {
    e.preventDefault()
    console.log(input)
  }

  const closeNewsletterPanel = () => {

    const now = new Date();
    const expired = now.setMinutes(now.getMinutes() + 10);
    const config = {
      closed: hide,
      subscribe: false,
      expired
    };

    setHide(true)
    setYTranslate(`${node.current.scrollHeight}`)
    storage.setItem('panel', JSON.stringify(config))

  }

  return (
    <React.Fragment>
      <div
        className="newsletter-container"
        style={{ transform: `translateY(${yTranslate}px)`, bottom: `-${node.current && node.current.scrollHeight}px` }}
        ref={node}>
        <Card>
          <div className="newsletter-title">
            <div
              className="close-panel"
              onClick={closeNewsletterPanel}>
              x
            </div>
            <h3>Get latest updates in web technologies </h3>
          </div>
          <div className="newsletter-body-content">
            <p className="newsletter-body-content-text">
              I write articles related to web technologies, such as design trends, development
              tools, UI/UX case studies and reviews, and more. Sign up to my newsletter to get
              them all.
            </p>
            <form onSubmit={handleFormSubmit}>
              <div className="input-group inline-input">
                <div className="input-wrapper">
                  <input
                    type="email"
                    className="input"
                    placeholder="Email address"
                    value={input}
                    onChange={handleOnChange}
                    required />
                </div>
                <div style={{ marginBottom: '1rem' }} className="mobile">
                  <button
                    className="btn btn-inline bg-orange btn-block">
                    Count me in!
                  </button>
                </div>
              </div>
            </form>
          </div>
        </Card>
      </div>
    </React.Fragment>
  )
}

export default NewsLetter
