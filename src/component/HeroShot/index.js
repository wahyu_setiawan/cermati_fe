import React from 'react'
import CermatiLogo from '../../asset/img/cermati-logo.png'
import './styles.css'

const HeroShot = () => {
  return (
    <div className="heroshot-container">
      <div className="heroshot-bg" />
      <div className="logo-wrapper">
        <div className="logo">
          <img src={CermatiLogo} alt="Cermati Logo" className="image-logo" />
        </div>
      </div>
      <div className="heroshot-content">
        <div className="header-content">
          Hello! I'm Wahyu setiawan
        </div>
        <div className="header-content">
          Consult, Design, and Develop Website
        </div>
        <div className="desc-content">
          <div className="desc-text">
            Have something great in mind ? Feel free to contact me.
          </div>
          <div className="desc-text">
            I'll help you to make it happen.
          </div>
        </div>

        <div className="footer-content">
          <div className="btn btn-border btn-inline btn-hover">
            Let's make contact
          </div>
        </div>
      </div>
    </div>
  )
}

export default HeroShot
