import React from 'react'
import PropTypes from 'prop-types'
import './index.css'

const style = {
  card: {
    display: 'block',
    flex: 1
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    margin: '.2rem 0 1rem 0'
  }
}

const Card = ({
  children,
  title,
  icon,
  body,
  footer,
  isDefault,
  className,
}) => {
  const classname = ("string" === typeof className)
    ? className
    : ('object' === typeof className)
      ? className.toString().split(',').join(' ')
      : ''

  return (
    <div style={style.card} className={isDefault ? `card-component default` : `card-component ${classname}`}>
      <div style={style.title}>
        {title ? <h3 className="title">{title}</h3> : null}
        {icon ? <span className="withIcon">{icon}</span> : null}
      </div>
      <div className="card-body">
        {children
          ? children
          : body
            ? <div className="card-body">
              <div className="text-body">{body}</div>
            </div>
            : null
        }
      </div>
      <div>
        {footer ? footer : null}
      </div>
    </div>
  )
}

export default Card

Card.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.element,
  body: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  footer: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  isDefault: PropTypes.bool,
  className: PropTypes.oneOf([PropTypes.string, PropTypes.array])
}
